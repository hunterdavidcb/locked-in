﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHealth : MonoBehaviour
{
	Image image;
    // Start is called before the first frame update
    void Start()
    {
		image = GetComponent<Image>();
		GameObject.FindGameObjectWithTag("Player").
			GetComponent<Health>().PlayerHealthChanged += OnHealthChanged;
    }

	private void OnHealthChanged(float c)
	{
		image.fillAmount = c / 100f;
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
