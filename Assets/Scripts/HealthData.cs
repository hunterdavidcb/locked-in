﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New HealthData")]
public class HealthData : ScriptableObject
{
	[Range(0,200)]
	public float health;

	public bool damagedByKeys;
}
