﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Health : MonoBehaviour
{
	public HealthData healthData;

	private float maxHealth;
	private float currentHealth;

	bool isDead = false;

	public bool isBoss;

	public delegate void BossDeathHandler();
	public event BossDeathHandler BossDied;

	public delegate void PlayerDeathHandler();
	public event PlayerDeathHandler PlayerDied;

	public delegate void PlayerHelathHandler(float c);
	public event PlayerHelathHandler PlayerHealthChanged;

	private void Awake()
	{
		maxHealth = currentHealth = healthData.health;
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.tag == "Key" && healthData.damagedByKeys && !isDead)
		{
			Key key = collision.collider.GetComponentInParent<Key>();
			if (!key.collided)
			{
				KeyData data = key.keyData;
				ReceiveDamage(data.damage);
			}
			
		}
	}

	public void ReceiveDamage(float d)
	{
		currentHealth -= d;
		if (gameObject.tag.Equals("Player"))
		{
			if (PlayerHealthChanged != null)
			{
				PlayerHealthChanged(currentHealth);
			}
		}

		if (currentHealth <= 0f)
		{
			Debug.Log("dead");
			isDead = true;
			NavMeshAgent agent = GetComponent<NavMeshAgent>();
			if (agent)
			{
				agent.enabled = false;
			}
			Rigidbody rb = GetComponent<Rigidbody>();
			rb.isKinematic = false;
			rb.constraints = RigidbodyConstraints.None;

			if (gameObject.tag.Equals("Player"))
			{
				GetComponent<ShootKey>().enabled = false;
				GetComponent<CameraControl>().enabled = false;
			}

			Invoke("Die", 2f);
		}
	}

	private void Die()
	{
		if (isBoss)
		{
			if (BossDied != null)
			{
				BossDied();
			}
		}

		if (gameObject.tag.Equals("Player"))
		{
			if (PlayerDied != null)
			{
				PlayerDied();
			}
		}
		else
		{
			Destroy(gameObject);
		}
		
	}
}
