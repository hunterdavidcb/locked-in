﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
	public int maxKeyOne = 100;
	public int currentKeyOne;

	public int maxKeyTwo = 100;
	public int currentKeyTwo;
	public int currentKeyThree;
	public  int maxKeyThree = 100;
	public int maxKeyFour = 100;
	public int currentKeyFour;

	public delegate void InventoryChangeHandler(string kd, int cur);
	public event InventoryChangeHandler InventoryChanged;

	private void Awake()
	{
		
	}
	// Start is called before the first frame update
	void Start()
    {
		GetComponent<ShootKey>().KeyShot += OnKeyShot;
		currentKeyOne = maxKeyOne;
		currentKeyTwo = maxKeyTwo;
		currentKeyThree = maxKeyThree;
		currentKeyFour = maxKeyFour;

		if (InventoryChanged != null)
		{
			InventoryChanged("KeyOne", currentKeyOne);
		}

		if (InventoryChanged != null)
		{
			InventoryChanged("KeyTwo", currentKeyTwo);
		}

		if (InventoryChanged != null)
		{
			InventoryChanged("KeyThree", currentKeyThree);
		}

		if (InventoryChanged != null)
		{
			InventoryChanged("KeyFour", currentKeyFour);
		}

		GetComponent<PickUp>().KeyPickedUp += OnKeyPickedUp;
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	protected void OnKeyShot(KeyData kd)
	{
		if (kd.name.Equals("KeyOne"))
		{
			currentKeyOne--;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyOne);
			}
		}

		if (kd.name.Equals("KeyTwo"))
		{
			currentKeyTwo--;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyTwo);
			}
		}

		if (kd.name.Equals("KeyThree"))
		{
			currentKeyThree--;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyThree);
			}
		}

		if (kd.name.Equals("KeyFour"))
		{
			currentKeyFour--;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyFour);
			}
		}


	}

	protected void OnKeyPickedUp(GameObject go)
	{
		KeyData kd = go.GetComponent<Key>().keyData;

		if (kd.name.Equals("KeyOne") && currentKeyOne < maxKeyOne)
		{
			currentKeyOne++;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyOne);
			}
			Destroy(go);
		}

		if (kd.name.Equals("KeyTwo") && currentKeyTwo < maxKeyTwo)
		{
			currentKeyTwo++;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyTwo);
			}
			Destroy(go);
		}

		if (kd.name.Equals("KeyThree") && currentKeyThree < maxKeyThree)
		{
			currentKeyThree++;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyThree);
			}
			Destroy(go);
		}

		if (kd.name.Equals("KeyFour") && currentKeyFour < maxKeyFour)
		{
			currentKeyFour++;
			if (InventoryChanged != null)
			{
				InventoryChanged(kd.name, currentKeyFour);
			}
			Destroy(go);
		}



	}
}
