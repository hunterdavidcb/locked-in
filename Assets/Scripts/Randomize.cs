﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomize : MonoBehaviour
{
	public MeshRenderer cylOne;
	public MeshRenderer cylTwo;
	public MeshRenderer cylThree;
	// Start is called before the first frame update
	void Awake()
    {
		Debug.Log("Randomizing");
		cylOne.material.SetTextureOffset("_MainTex", new Vector2(Random.Range(-1f, 1f), -.22f));
		cylTwo.material.SetTextureOffset("_MainTex", new Vector2(Random.Range(-1f, 1f), -.22f));
		cylThree.material.SetTextureOffset("_MainTex", new Vector2(Random.Range(-1f, 1f), -.22f));
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
