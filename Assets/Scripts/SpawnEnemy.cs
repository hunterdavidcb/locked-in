﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
	public Transform spawnPoint;

	public GameObject[] enemies;
	public float difficulty;
	public int numToSpawn;
	public float delay;
    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	IEnumerator Spawn()
	{
		for (int i = 0; i < numToSpawn; i++)
		{
			yield return new WaitForSeconds(delay);
			int index = Random.Range(0, enemies.Length);
			switch (difficulty)
			{
				case 1:
					break;
				case 2:
					if (index < enemies.Length - 1)
					{
						index++;
					}
					break;
				case 3:
					if (index < enemies.Length - 2)
					{
						index+= Random.Range(1,3);
					}
					break;
				case 4:
					if (index < enemies.Length - 3)
					{
						index += Random.Range(1, 4);
					}
					break;
				case 5:
					if (index < enemies.Length - 3)
					{
						index += Random.Range(1, 4);
					}
					break;
				default:
					break;
			}

			GameObject enemy = Instantiate(enemies[index], transform);
			enemy.transform.position = transform.position;
		}
		yield return null;
	}
}
