﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CurvedText : Text
{

	public float radius = 0.5f;
	public float wrapAngle = 360f;
	public float scaleFactor = 100f;

	private float circumference
	{
		get
		{
			if (_radius != radius || _scaleFactor != scaleFactor)
			{
				_circumference = 2f * Mathf.PI * radius * scaleFactor;
				_radius = radius;
				_scaleFactor = scaleFactor;
			}

			return _circumference;
		}
	}

	private float _radius = -1f;
	private float _scaleFactor = -1f;
	private float _circumference = -1f;

#if UNITY_EDITOR
	protected override void OnValidate()
	{
		base.OnValidate();
		if (radius <= 0f)
		{
			radius = 0.001f;
		}

		if (scaleFactor <= 0f)
		{
			scaleFactor = 0.001f;
		}
	}
#endif

	//protected override void OnFillVBO(List<UIVertex> vbo)
	//{
	//	base.OnFillVBO(vbo);

	//	for (int i = 0; i < vbo.Count; i++)
	//	{
	//		UIVertex v = vbo[i];

	//		Vector3 p = v.position;

	//		float percentCircumference = v.position.x / circumference;
	//		Vector3 offset = Quaternion.Euler(0f, 0f, -percentCircumference * 360f) * Vector3.up;
	//		p = offset * radius * scaleFactor + offset * v.position.y;
	//		p += Vector3.down * radius * scaleFactor;

	//		v.position = p;

	//		vbo[i] = v;
	//	}

	//}

	protected override void OnPopulateMesh(VertexHelper m)
	{
		base.OnPopulateMesh(m);

		for (int i = 0; i < m.currentVertCount; i++)
		{
			UIVertex v = new UIVertex();
			m.PopulateUIVertex(ref v, i);

			Vector3 p = v.position;

			float percentCircumference = v.position.x / circumference;
			Vector3 offset = Quaternion.Euler(0f, 0f, -percentCircumference * 360f) * Vector3.up;
			//p = offset * radius * scaleFactor + offset * v.position.y;
			p = offset * radius * scaleFactor + offset * v.position.y;
			p += Vector3.down * radius * scaleFactor;

			v.position = p;

			m.SetUIVertex(v,i);
		}
	}

    // Update is called once per frame
    void Update()
    {
		if (radius <= 0f)
		{
			radius = 0.001f;
		}

		if (scaleFactor <= 0f)
		{
			scaleFactor = 0.001f;
		}

		rectTransform.sizeDelta = new Vector2(circumference * wrapAngle / 360f, rectTransform.sizeDelta.y);
	}
}
