﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeekPlayer : MonoBehaviour
{
	NavMeshAgent agent;

	Transform player;

	public AttackData attackData;
	float lastAttack;

	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
		player = GameObject.FindGameObjectWithTag("Player").transform;
		agent.SetDestination(player.position);
		lastAttack = Time.time;
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		OnPlayerPositionUpdated();

		if (Vector3.Distance(player.position, transform.position) < 2f 
			&& Time.time - lastAttack >= attackData.time)
		{
			Attack();
			lastAttack = Time.time;
		}
    }

	void OnPlayerPositionUpdated()
	{
		if (player.hasChanged && agent.enabled)
		{
			agent.SetDestination(player.position);
		}
	}

	void Attack()
	{
		player.GetComponent<Health>().
			ReceiveDamage(Random.Range(attackData.minDamage, attackData.maxDamage));
		FMODUnity.RuntimeManager.PlayOneShot(attackData.sounds);
		//Debug.Log("attacking");
	}
}
