﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
	public delegate void KeyEventHandler(GameObject go);
	public event KeyEventHandler KeyPickedUp;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.rigidbody != null)
		{
			if (collision.rigidbody.tag == "Key")
			{
				if (KeyPickedUp != null)
				{
					KeyPickedUp(collision.rigidbody.gameObject);
				}
			}
		}
		
	}
}
