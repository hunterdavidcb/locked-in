﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New KeyData")]
public class KeyData : ScriptableObject
{
	public float damage;
}
