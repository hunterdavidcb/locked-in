﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateInventory : MonoBehaviour
{
	CurvedText text;
	Dictionary<string, int> items = new Dictionary<string, int>();
	int currentKey = 0;
	private void Awake()
	{
		text = GetComponent<CurvedText>();
		FindObjectOfType<Inventory>().InventoryChanged += OnInventoryChanged;
		FindObjectOfType<ShootKey>().CurrentKeyChanged += OnCurrentKeyChanged;
	}

	private void OnCurrentKeyChanged(int i)
	{
		currentKey = i;
		UpdateInventoryText();
	}

	private void OnInventoryChanged(string kd, int cur)
	{
		if (items.ContainsKey(kd))
		{
			items[kd] = cur;
		}
		else
		{
			items.Add(kd, cur);
		}

		UpdateInventoryText();
	}

	private void UpdateInventoryText()
	{
		text.text = "";
		foreach (var item in items)
		{
			if (currentKey == 1 && item.Key.Contains("One"))
			{
				text.text += "<b>" + item.Key + ": " +
							item.Value.ToString() + "</b>\t";
			}
			else if (currentKey == 2 && item.Key.Contains("Two"))
			{
				text.text += "<b>" + item.Key + ": " +
							item.Value.ToString() + "</b>\t";
			}
			else if (currentKey == 3 && item.Key.Contains("Three"))
			{
				text.text += "<b>" + item.Key + ": " +
							item.Value.ToString() + "</b>\t";
			}
			else if (currentKey == 4 && item.Key.Contains("Four"))
			{
				text.text += "<b>" + item.Key + ": " +
							item.Value.ToString() + "</b>\t";
			}
			else
			{
				text.text += item.Key + ": " +
							item.Value.ToString() + "\t";
			}
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
