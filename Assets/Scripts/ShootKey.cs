﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootKey : MonoBehaviour
{
	public Transform keyPrefab;
	public Transform keyTwoPrefab;
	public Transform keyThreePrefab;
	public Transform keyFourPrefab;
	float reset = 0.5f;
	float current = 0f;
	new Transform camera;

	[FMODUnity.EventRef]
	public string shoot;

	public delegate void KeyEventHandler(KeyData kd);
	public event KeyEventHandler KeyShot;

	public int currentKey = 1;


	Inventory inventory;

	public delegate void CurrentKeyHandler(int i);
	public event CurrentKeyHandler CurrentKeyChanged;
	// Start is called before the first frame update
	void Start()
    {
		camera = transform.Find("Main Camera");
		inventory = GetComponent<Inventory>();
		if (CurrentKeyChanged != null)
		{
			CurrentKeyChanged(currentKey);
		}
	}

    // Update is called once per frame
    void Update()
    {
		if (Input.GetMouseButton(0) && Time.time - current >= reset)
		{
			GameObject key;
			switch (currentKey)
			{
				case 1:
					if (inventory.currentKeyOne > 0)
					{
						key = Instantiate(keyPrefab.gameObject,
							transform.position + Vector3.up + transform.forward, camera.rotation);
						AssignKeyStuff(key);
					}
					break;
				case 2:
					if (inventory.currentKeyTwo > 0)
					{
						key = Instantiate(keyTwoPrefab.gameObject,
							transform.position + Vector3.up + transform.forward, camera.rotation);
						AssignKeyStuff(key);
					}
					break;
				case 3:
					if (inventory.currentKeyThree > 0)
					{
						key = Instantiate(keyThreePrefab.gameObject,
							transform.position + Vector3.up + transform.forward, camera.rotation);
						AssignKeyStuff(key);
					}
					break;
				case 4:
					if (inventory.currentKeyFour > 0)
					{
						key = Instantiate(keyFourPrefab.gameObject,
							transform.position + Vector3.up + transform.forward, camera.rotation);
						AssignKeyStuff(key);
					}
					break;

				default:
					break;
			}			
		}

		//Debug.Log(Input.GetAxis("Mouse ScrollWheel"));
		if (Input.GetAxis("Mouse ScrollWheel") > 0.05f)
		{
			currentKey++;
			if (currentKey > 4)
			{
				currentKey = 1;
			}

			if (CurrentKeyChanged != null)
			{
				CurrentKeyChanged(currentKey);
			}
		}
		else if (Input.GetAxis("Mouse ScrollWheel") < -0.05f)
		{
			currentKey--;
			if (currentKey < 1)
			{
				currentKey = 4;
			}
			if (CurrentKeyChanged != null)
			{
				CurrentKeyChanged(currentKey);
			}
		}
	}

	void AssignKeyStuff(GameObject key)
	{
		key.GetComponent<Rigidbody>().AddForce(camera.forward * 1000);
		current = Time.time;
		if (KeyShot != null)
		{
			KeyShot(key.GetComponent<Key>().keyData);
		}
		FMODUnity.RuntimeManager.PlayOneShot(shoot);
	}
}
