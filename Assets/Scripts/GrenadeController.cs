﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeController : MonoBehaviour
{
	public float lifeTime = 5f;
	bool explode = false;
	ParticleSystem ps;

	HashSet<GameObject> canDamage = new HashSet<GameObject>();

	private void Awake()
	{
		ps = GetComponentInChildren<ParticleSystem>();
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		lifeTime -= Time.deltaTime;

		if (lifeTime <= 0 && !explode)
		{
			explode = true;
			Explode();
			foreach (var l in canDamage)
			{
				l.GetComponent<Health>().ReceiveDamage(GetComponent<Key>().keyData.damage);
			}
			Invoke("SelfDestruct", 2f);
		}
    }

	void Explode()
	{
		ps.Play();
	}

	void SelfDestruct()
	{
		Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{
			if (!canDamage.Contains(other.transform.root.gameObject))
			{
				canDamage.Add(other.transform.root.gameObject);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Enemy")
		{
			if (canDamage.Contains(other.transform.root.gameObject))
			{
				canDamage.Remove(other.transform.root.gameObject);
			}
		}
	}


}
