﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
	public int nextScene;

	public GameObject panel;

	public GameObject failurePanel;

	private void Awake()
	{
		Health[] h = FindObjectsOfType<Health>();
		foreach (var he in h)
		{
			he.BossDied += OnBossDeath;
			he.PlayerDied += OnPlayerDeath;
		}

		panel.SetActive(false);
		failurePanel.SetActive(false);
	}

	private void OnPlayerDeath()
	{
		failurePanel.SetActive(true);
	}

	public void LoadMain()
	{
		SceneManager.LoadScene(0);
	}

	private void OnBossDeath()
	{
		panel.SetActive(true);
		FindObjectOfType<ShootKey>().enabled = false;
		FindObjectOfType<CameraControl>().enabled = false;
		//throw new NotImplementedException();
	}

	public void LoadNext()
	{
		SceneManager.LoadScene(nextScene);
	}
}
