﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Attack")]
public class AttackData : ScriptableObject
{
	public float minDamage;
	public float maxDamage;
	public float time;
	[FMODUnity.EventRef]
	public string sounds;
}
