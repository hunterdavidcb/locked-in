﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
	new Transform camera;

	public float sensitivityX = 15F;
	public float sensitivityY = 15F;
	public float minimumX = -360F;
	public float maximumX = 360F;
	public float minimumY = -60F;
	public float maximumY = 60F;
	float rotationY = 0F;

	public float speed = 10f;

	private void Awake()
	{
		camera = transform.Find("Main Camera");
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		
		float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

		rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
		rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

		camera.localEulerAngles = new Vector3(-rotationY, 0f, 0);

		transform.localEulerAngles = new Vector3(0f, rotationX, 0f);

		Move();
	}

	void Move()
	{
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");

		Vector3 delta = new Vector3(x, 0f ,y) * speed * Time.deltaTime;
		transform.Translate(delta,Space.Self);
	}
}
