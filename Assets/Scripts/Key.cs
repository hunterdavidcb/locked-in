﻿using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour
{
	public KeyData keyData;
	Rigidbody rb;
	public bool collided = false;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		
	}

	private void LateUpdate()
	{
		if (rb.velocity != Vector3.zero && !collided)
		{
			transform.rotation = Quaternion.LookRotation(rb.velocity, Vector3.up);
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		Invoke("SetCollided", .1f);
	}

	private void SetCollided()
	{
		collided = true;
	}
}
