﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Copyright : MonoBehaviour
{
	public string text;
	// Use this for initialization
	void Start ()
	{
		GetComponent<Text>().text = "\u00A9" + " 2020 " + text.Replace(@"\n","\n");
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
